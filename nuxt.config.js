module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'Full-stack Web Developer in Tokyo',
        titleTemplate: 'Romain Leger | %s',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [{
            charset: 'utf-8'
        },
        {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1'
        }, {
            vmid: "description",
            name: "description",
            content: "I work with web agencies and startups to develop modern websites & progressive web applications using the latest technologies."
        },
        {
            vmid: "og:title",
            property: "og:title",
            content: "Romain Leger | Full-stack Web Developer in Tokyo"
        },
        {
            vmid: "og:description",
            property: "og:description",
            content: "I work with web agencies and startups to develop modern websites & progressive web applications using the latest technologies."
        },
        {
            vmid: "og:site_name",
            property: "og:site_name",
            content: "Romain Leger | Full-stack Web Developer in Tokyo"
        },
        {
            vmid: "og:url",
            property: "og:url",
            content: "https://leger-romain.com/"
        },
        {
            vmid: "og:image",
            property: "og:image",
            content: "https://leger-romain.com/share.jpg"
        },
        {
            vmid: "og:type",
            property: "og:type",
            content: "website"
        }
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        },
        {
            rel: 'stylesheet',
            href: 'https://fonts.googleapis.com/css?family=Lato'
        }
        ]
    },
    /*
     ** Customize the progress bar color
     */
    loading: {
        color: '#3B8070'
    },
    /*
     ** Nuxt.js modules
     */
    modules: [
        'nuxt-purgecss',
    ],
    /*
     ** Build configuration
     */
    build: {
        /*
         ** Run ESLint on save
         */
        postcss: [
            require('tailwindcss')('./tailwind.js'),
            require('autoprefixer')
        ],
        extractCSS: {
            allChunks: true
        },
        extend(config, {
            isDev,
            isClient
        }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    css: ['~/assets/css/tailwind.css'],
    plugins: [
        {
            src: '~plugins/ga.js',
            ssr: false
        },
        {
            src: '~/plugins/intersection-observer.js',
            ssr: false
        },
    ]
}